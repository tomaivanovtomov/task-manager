<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\TaskController;
use App\Http\Controllers\BoardsController;
use App\Http\Controllers\Auth\LoginController;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'index']);

Auth::routes();

Route::get('/password/email', function () {
        return view('auth.passwords.email');
})->middleware('guest');

Route::group([
        'middleware' => 'is_admin'
    ], function () {
        Route::get('/roles', [RoleController::class, 'index'])->name('roles.index');
        Route::post('/users/delete', [UserController::class, 'delete'])->name('users.delete');
        Route::post('/users/restore', [UserController::class, 'restore'])->name('users.restore');
        Route::get('/users/edit/{id}', [UserController::class, 'edit'])->name('users.edit');
        Route::post('/users/update', [UserController::class, 'update'])->name('users.update');
        Route::post('/boards/create', [BoardsController::class, 'create'])->name('boards.create');
        Route::post('/boards/delete', [BoardsController::class, 'delete'])->name('boards.delete');
        Route::post('/boards/restore', [BoardsController::class, 'restore'])->name('boards.restore');
});

Route::group([
    'middleware' => 'is_manager'
], function() {
    Route::get('/users', [UserController::class, 'index'])->name('users.index');
    Route::post('/tasks/create', [TaskController::class, 'create'])->name('tasks.create');
    Route::post('/boards/edit', [BoardsController::class, 'edit'])->name('boards.edit');
    Route::post('/boards/update', [BoardsController::class, 'update'])->name('boards.update');
    Route::post('/boards/order', [BoardsController::class, 'order'])->name('boards.order');
    Route::post('/tasks/delete', [TaskController::class, 'delete'])->name('tasks.delete');
    Route::post('/tasks/restore', [TaskController::class, 'restore'])->name('tasks.restore');
    Route::post('/tasks/edit', [TaskController::class, 'edit'])->name('tasks.edit');
    Route::post('/tasks/update', [TaskController::class, 'update'])->name('tasks.update');
});

Route::group([
    'middleware' => 'is_user'
], function() {
    Route::get('/tasks', [TaskController::class, 'index'])->name('tasks.index');
    Route::post('/tasks/order', [TaskController::class, 'order'])->name('tasks.order');
    Route::get('/login/logout', [LoginController::class, 'logout'])->name('logout');
});
