<?php

namespace Database\Factories;

use App\Models\Board;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class TaskFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $board = Board::factory()->create();
        $doer = User::factory()->create();
        $creator = User::factory()->create();
        return [
            'title' => $this->faker->title(),
            'description' => 'some desc',
            'creator_id' => $creator->id,
            'board_id' => $board->id,
            'doer_id' => $doer->id,
            'order' => 1,
            'deleted' => null
        ];
    }
}
