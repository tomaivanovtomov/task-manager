<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class BoardFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title' => $this->faker->name(),
            'board_id' => 'to_do_123',
            'drag_to' => 'in_pr_1',
            'entry' => null,
            'order' => 1,
            'deleted' => null
        ];
    }
}
