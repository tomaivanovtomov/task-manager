<?php

namespace Tests\Feature;

use App\Models\Board;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class BoardTest extends TestCase
{
    public function test_board_create()
    {
        $user = User::factory()->create();
        if ($user->role_id !== 1) {
            $this->assertFalse('No permissions', 'Board - Forbidden 403');
        }
        $this->actingAs($user);
        $r = $this->get('/tasks');
        if ($r->status() === 200) {
            $response = $this->post('/boards/create', [
                'title' => 'to do',
                'drag_to' => ['in_pr_1'],
                'entry' => 1
            ]);
            $response->assertRedirect('/tasks');
        } else {
            $this->assertTrue($r->status());
        }
        return $user;
    }

    /**
     * @depends test_board_create
     */
    public function test_board_update(User $user)
    {
        $board = Board::factory()->create();
        if ($user->role_id !== 1) {
            $this->assertTrue('No permissions', 'Board - Forbidden 403');
        }
        $this->actingAs($user);
        if ($this->get('/tasks')->status() === 200) {
            $response = $this->post('/boards/update', [
                'id' => $board->id,
                'title' => 'to do1',
                'drag_to' => ['in_pr_1', 'in_pr_2'],
                'entry' => null
            ]);
            $response->assertRedirect('/tasks');
        } else {
            $this->assertTrue(200, 'Board - Route error');
        }
        return $board;
    }

    /**
     * @depends test_board_create
     * @depends test_board_update
     */
    public function test_board_delete(User $user, Board $board)
    {
        if ($user->role_id !== 1) {
            $this->assertTrue('No permissions', 'Board - Forbidden 403');
        }
        $this->actingAs($user);
        if ($this->get('/tasks')->status() === 200) {
            $response = $this->post('/boards/delete', [
                'id' => $board->id
            ]);
            if ($response->status() === 200) {
                $this->assertTrue(true);
            } else {
                $this->assertTrue(false, 'Board - Incorrect response status code');
            }
        } else {
            $this->assertTrue(200, 'Route error');
        }
    }

    /**
     * @depends test_board_create
     * @depends test_board_update
     */
    public function test_board_drag(User $user, Board $board)
    {
        if ($user->role_id !== 1) {
            $this->assertTrue('No permissions', 'Board - Forbidden 403');
        }
        $this->actingAs($user);
        if ($this->get('/tasks')->status() === 200) {
            $response = $this->post('/boards/order', [
                'data' => [
                    [
                        $board->board_id,
                        1
                    ]
                ]
            ]);
            if ($response->status() === 200) {
                $this->assertTrue(true);
            } else {
                $this->assertTrue(false, 'Board - Incorrect response status code');
            }
        } else {
            $this->assertTrue(200, 'Board - Route error');
        }
    }
}
