<?php

namespace Tests\Feature;

use App\Models\User;
use Tests\TestCase;

class RoleTest extends TestCase
{
    public function test_rendering_roles_page_as_admin()
    {
        $user = User::factory()->create();
        $this->actingAs($user);
        $responst = $this->get('/roles');
        $responst->assertStatus(200);
        return $user;
    }

    /**
     * @depends test_rendering_roles_page_as_admin
     */
    public function test_rendering_roles_page_as_manager(User $user)
    {
        $user->role_id = 2;
        $this->actingAs($user);
        $responst = $this->get('/roles');
        $responst->assertStatus(403);
    }

    /**
     * @depends test_rendering_roles_page_as_admin
     */
    public function test_rendering_roles_page_as_user(User $user)
    {
        $user->role_id = 3;
        $this->actingAs($user);
        $responst = $this->get('/roles');
        $responst->assertStatus(403);
    }
}
