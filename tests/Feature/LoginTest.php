<?php

namespace Tests\Feature;

use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use MongoDB\Driver\Session;
use Tests\TestCase;

class LoginTest extends TestCase
{
    public function test_register_screen()
    {
        $response = $this->get('/login');
        $response->assertStatus(200);
    }

    public function test_user_login()
    {
        $response = $this->post('/login', [
            'email' => 'tomaivanovtomov@gmail.com',
            'password' => '12341234'
        ]);

        $this->assertAuthenticated();
        $response->assertRedirect(RouteServiceProvider::HOME);
    }
}
