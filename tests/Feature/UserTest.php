<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UserTest extends TestCase
{
    public function test_rendering_users_page_as_admin()
    {
        $user = User::factory()->create();
        $this->actingAs($user);
        $responst = $this->get('/users');
        $responst->assertStatus(200);
        return $user;
    }

    /**
     * @depends test_rendering_users_page_as_admin
     */
    public function test_rendering_users_page_as_manager(User $user)
    {
        $user->role_id = 2;
        $this->actingAs($user);
        $responst = $this->get('/users');
        $responst->assertStatus(200);
    }

    /**
     * @depends test_rendering_users_page_as_admin
     */
    public function test_rendering_users_page_as_user(User $user)
    {
        $user->role_id = 3;
        $this->actingAs($user);
        $responst = $this->get('/users');
        $responst->assertStatus(403);
        $user->role_id = 1;
    }

    /**
     * @depends test_rendering_users_page_as_admin
     */
    public function test_render_user_edit_page(User $user)
    {
        $this->actingAs($user);
        $response = $this->call('GET', '/users/edit/'.$user->id);
        $response->assertStatus(200);
    }

    /**
     * @depends test_rendering_users_page_as_admin
     */
    public function test_updating_user_data(User $user)
    {
        $this->actingAs($user);
        $response = $this->get('/users/edit/'.$user->id);
        if ($response->status() === 200) {
            $updateResponse = $this->post('/users/update', [
                'id' => $user->id,
                'password' => '12341234',
                'password_confirmation' => '123412345',
                'name' => 'testname',
                'email' => 'test@example.com',
                'role_id' => 1
            ]);
            $updateResponse->assertRedirect('/users/edit/'.$user->id);
        } else {
          $this->assertFalse();
        }
    }
}
