<?php

namespace Tests\Feature;

use App\Models\Board;
use App\Models\Task;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class TaskTest extends TestCase
{
    public function test_task_create()
    {
        $user = User::factory()->create();
        $board = Board::factory()->create();
        if ($user->role_id === 3) {
            $this->assertFalse('No permissions', 'Task - Forbidden 403');
        }
        $this->actingAs($user);
        $r = $this->get('/tasks');
        if ($r->status() === 200) {
            $response = $this->post('/tasks/create', [
                'title' => 'task title',
                'description' => 'task desc',
                'creator_id' => $user->id,
                'doer_id' => $user->id,
                'board_id' => $board->id
            ]);
            $response->assertRedirect('/tasks');
        } else {
            $this->assertTrue($r->status());
        }
        return $user;
    }

    /**
     * @depends test_task_create
     */
    public function test_task_update(User $user)
    {
        $task = Task::factory()->create();
        $board = Board::factory()->create();
        if ($user->role_id === 3) {
            $this->assertTrue('No permissions', 'Task - Forbidden 403');
        }
        $this->actingAs($user);
        if ($this->get('/tasks')->status() === 200) {
            $response = $this->post('/tasks/update', [
                'id' => $task->id,
                'data' => [
                    'title' => 'task title upd',
                    'description' => 'task desc upd',
                    'doer_id' => $user->id,
                    'board_id' => $board->id
                ]
            ]);
            $response->assertRedirect('/tasks');
        } else {
            $this->assertTrue(200, 'Task - Route error');
        }
        return $task;
    }

    /**
     * @depends test_task_create
     * @depends test_task_update
     */
    public function test_task_delete(User $user, Task $task)
    {
        if ($user->role_id === 3) {
            $this->assertTrue('No permissions', 'Task - Forbidden 403');
        }
        $this->actingAs($user);
        if ($this->get('/tasks')->status() === 200) {
            $response = $this->post('/tasks/delete', [
                'id' => $task->id
            ]);
            if ($response->status() === 200) {
                $this->assertTrue(true);
            } else {
                $this->assertTrue(false, 'Task - Incorrect response status code');
            }
        } else {
            $this->assertTrue(200, 'Route error');
        }
    }

    /**
     * @depends test_task_create
     * @depends test_task_update
     */
    public function test_task_drag(User $user, Task $task)
    {
        $board = Board::factory()->create();
        if ($user->role_id !== 1) {
            $this->assertTrue('No permissions', 'Task - Forbidden 403');
        }
        $this->actingAs($user);
        if ($this->get('/tasks')->status() === 200) {
            $response = $this->post('/tasks/order', [
                'data' => [
                    [
                        $task->id,
                        $board->id
                    ]
                ]
            ]);
            if ($response->status() === 200) {
                $this->assertTrue(true);
            } else {
                $this->assertTrue(false, 'Task - Incorrect response status code');
            }
        } else {
            $this->assertTrue(200, 'Task - Route error');
        }
    }
}
