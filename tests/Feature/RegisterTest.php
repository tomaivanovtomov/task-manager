<?php

namespace Tests\Feature;

use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class RegisterTest extends TestCase
{
    public function test_register_screen()
    {
        $response = $this->get('/register');
        $response->assertStatus(200);
    }

    public function test_user_register()
    {
        $response = $this->post('/login', [
            'name' => 'tomatomov',
            'email' => 'tomaivanovtomov@gmail.com',
            'password' => '12341234',
            'password_confirmation' => '12341234',
            'terms' => true
        ]);

        $this->assertAuthenticated();
        $response->assertRedirect(RouteServiceProvider::HOME);
    }
}
