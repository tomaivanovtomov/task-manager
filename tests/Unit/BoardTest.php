<?php

namespace Tests\Unit;

use App\Models\Board;
use App\Models\Task;
use Tests\TestCase;

class BoardTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_board_tasks_relations()
    {
        $board = Board::factory()->create();
        $task = Task::factory()->create();
        $task->board_id = $board->id;
        $this->assertTrue((bool)$board->tasks);
        return $board;
    }

    /**
     * @depends test_board_tasks_relations
     */
    public function test_entry_board(Board $board)
    {
        $board->fill(['entry' => 1]);
        $board->save();
        $entry_board = Board::getEntryBoard();
        $this->assertTrue((bool)$entry_board);
    }

    /**
     * @depends test_board_tasks_relations
     */
    public function test_get_board_by_id(Board $board)
    {
        $this->assertTrue((bool)Board::getBoardById($board->id));
    }

    public function test_reset_entry_board()
    {
        Board::resetBoardEntries();
        $this->assertNull(Board::getEntryBoard());
    }

    /**
     * @depends test_board_tasks_relations
     */
    public function test_delete_board(Board $board)
    {
        Board::deleteBoard($board->id);
        $board = Board::getBoardById($board->id);
        $this->assertEquals(1, $board->deleted);
    }

    /**
     * @depends test_board_tasks_relations
     */
    public function test_restore_board(Board $board)
    {
        Board::restoreBoard($board->id);
        $board = Board::getBoardById($board->id);
        $this->assertNull($board->deleted);
    }

    /**
     * @depends test_board_tasks_relations
     */
    public function test_update_board(Board $board)
    {
        $board->fill(['title' => 'test board']);
        $board->save();
        $board = Board::getBoardById($board->id);
        $this->assertEquals('test board', $board->title);
    }

    /**
     * @depends test_board_tasks_relations
     */
    public function test_board_decode(Board $board)
    {
        $decode = $board->selectedBoardsDecode();
        $type = gettype($decode);
        $this->assertEquals('array', $type);
    }

}
