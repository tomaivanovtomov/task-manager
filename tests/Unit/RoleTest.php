<?php

namespace Tests\Unit;

use App\Models\Role;
use Tests\TestCase;

class RoleTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_users_relation()
    {
        $role = Role::find(1);
        $this->assertTrue((bool)$role->users);
    }
}
