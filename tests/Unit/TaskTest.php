<?php

namespace Tests\Unit;

use App\Models\Board;
use App\Models\Task;
use App\Models\User;
use Tests\TestCase;

class TaskTest extends TestCase
{
    public function test_user_relation()
    {
        $task = Task::factory()->create();
        $this->assertTrue((bool)$task->user);
        return $task;
    }

    /**
     * @depends test_user_relation
     */
    public function test_board_relation(Task $task)
    {
        $this->assertTrue((bool)$task->board);
    }

    public function test_create_task()
    {
        $creator = User::factory()->create();
        $doer = User::factory()->create();
        $board = Board::factory()->create();
        $board->fill(['entry' => 1]);
        $board->save();
        $task = Task::createTask('title', 'description', $doer->id, $creator->id);
        $this->assertTrue((bool)$task);
    }

    /**
     * @depends test_user_relation
     */
    public function test_get_task_by_id(Task $task)
    {
        $task = Task::getTaskById($task->id);
        $this->assertTrue((bool)$task);
    }

    /**
     * @depends test_user_relation
     */
    public function test_update_task_by_id(Task $task)
    {
        $task->fill(['title' => "test task"]);
        $task->save();
        $task = Task::getTaskById($task->id);
        $this->assertEquals('test task', $task->title);
    }

    /**
     * @depends test_user_relation
     */
    public function test_delete_task_by_id(Task $task)
    {
        Task::deleteTask($task->id);
        $task = Task::getTaskById($task->id);
        $this->assertEquals(1, $task->deleted);
    }

    /**
     * @depends test_user_relation
     */
    public function test_restore_task_by_id(Task $task)
    {
        $task->fill(['deleted' => 1]);
        $task->save();
        Task::restoreTask($task->id);
        $task = Task::getTaskById($task->id);
        $this->assertNull($task->deleted);
    }
}
