<?php

namespace Tests\Unit;

use App\Models\Task;
use App\Models\User;
use Database\Factories\UserFactory;
use Tests\TestCase;

class UserTest extends TestCase
{
    public function test_check_user_role_functionallity()
    {
        $user = User::factory()->create();
        $has_role = $user->hasRole([1,2,3]);
        $this->assertTrue($has_role);
        return $user;
    }

    /**
     * @depends test_check_user_role_functionallity
     */
    public function test_role_relationship(User $user)
    {
        $this->assertTrue((bool)$user->role);
    }

    /**
     * @depends test_check_user_role_functionallity
     */
    public function test_tasks_relationship(User $user)
    {
        $task = Task::factory()->create();
        $task->doer_id = $user->id;
        $this->assertTrue((bool)$user->tasks);
    }

    /**
     * @depends test_check_user_role_functionallity
     */
    public function test_get_user_by_id(User $user)
    {
        $this->assertTrue((bool)User::getUserById($user->id));
    }

    /**
     * @depends test_check_user_role_functionallity
     */
    public function test_user_update(User $user)
    {
        User::updateUser($user->id, ['name' => 'test name']);
        $user = User::getUserById($user->id);
        $this->assertEquals('test name', $user->name);
    }

}
