$(function () {

    const options = {
        width: '100%',
        placeholder: "Select option",
        allowClear: true
    }

    $(document).on('click', ".delete-record", function () {
        let cnf = confirm("Delete this user?")
        if (cnf) {
            let routeDelete = $(this).data('route-delete')
            let routeRestore = $(this).data('route-restore')
            let id = $(this).data('id')
            $.ajax({
                method: 'post',
                url: routeDelete,
                data: {id},
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            }).done(function (res) {
                res = JSON.parse(res)
                if (res.success) {
                    $(".row"+id).addClass('deleted-record')
                    let cell = $(".delete-cell-wrapper"+id)
                    cell.after("<td class='restore-cell-wrapper"+id+"'><i data-toggle=\"tooltip\" title=\"Restore\" data-id='"+id+"' data-route-restore='"+routeRestore+"' data-route-delete='"+routeDelete+"' class=\"restore-theme-button ti-check restore-record\"></i></td>")
                    cell.remove()
                } else {
                    alert('Something went wrong! Please, try again.')
                }
            })
        }
    })

    $(document).on('click', ".restore-record", function () {
        let cnf = confirm("Restore this user?")
        if (cnf) {
            let routeDelete = $(this).data('route-delete')
            let routeRestore = $(this).data('route-restore')
            let id = $(this).data('id')
            $.ajax({
                method: 'post',
                url: routeRestore,
                data: {id},
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            }).done(function (res) {
                res = JSON.parse(res)
                if (res.success) {
                    $(".row"+id).removeClass('deleted-record')
                    let cell = $(".restore-cell-wrapper"+id)
                    cell.after("<td class='delete-cell-wrapper"+id+"'><i data-toggle=\"tooltip\" title=\"Delete\" data-id='"+id+"' data-route-delete='"+routeDelete+"' data-route-restore='"+routeRestore+"' class=\"restore-theme-button ti-trash delete-record\"></i></td>")
                    cell.remove()
                } else {
                    alert('Something went wrong! Please, try again.')
                }
            })
        }
    })

    $(document).on('click', '.edit-board', function () {
        let id = $(this).data('id')
        let url = $(this).data('url')
        $.ajax({
            method: 'post',
            url: url,
            data: {id},
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        }).done(function (res) {
            res = JSON.parse(res)
            $("#editBoard").html(res).modal('show')
            let slct = $('.multiselect-edit')
            if (!slct.hasClass('select2-hidden-accessible')) {
                slct.select2(options);
            }
        })
    })

    $(document).on('click', '.delete-board', function () {
        let type = $(this).data('type')
        let cnf = confirm(type.charAt(0).toUpperCase() + type.slice(1) + " this board?")
        if (cnf) {
            let id = $(this).data('id')
            let url = $(this).data('url')
            $.ajax({
                method: 'post',
                url: url,
                data: {id},
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            }).done(function (res) {
                location.reload()
            })
        }
    })

    $(document).on('click', '.delete-task', function () {
        let type = $(this).data('type')
        let cnf = confirm(type.charAt(0).toUpperCase() + type.slice(1) + " this task?")
        if (cnf) {
            let id = $(this).data('id')
            let url = $(this).data('url')
            $.ajax({
                method: 'post',
                url: url,
                data: {id},
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            }).done(function (res) {
                location.reload()
            })
        }
    })

    $(document).on('click', '.edit-task', function () {
        let id = $(this).data('id')
        let url = $(this).data('url')
        $.ajax({
            method: 'post',
            url: url,
            data: {id},
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        }).done(function (res) {
            res = JSON.parse(res)
            $("#editTask").html(res).modal('show')
            let slct = $('.multiselect-edit')
            if (!slct.hasClass('select2-hidden-accessible')) {
                slct.select2(options);
            }
        })
    })

    $('#editBoard').on('hidden.bs.modal', function () {
        $('.multiselect').select2(options);
    })

    $('#editTask').on('hidden.bs.modal', function () {
        $('.multiselect').select2(options);
    })

    $('.multiselect').select2(options);

})
