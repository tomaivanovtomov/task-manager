<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\Validation\ValidatorAwareRule;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use App\Models\Board;
use Illuminate\Support\Facades\View;

class BoardsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function create(Request $request)
    {
        $validator = $this->validator($request->all());
        if ($validator->fails()) {
            return Redirect::route('tasks.index')->withErrors($validator);
        } else {
            $entry = $request->get('entry') == "on" ? 1 : null;
            if ($entry) {
                Board::resetBoardEntries();
            } else {
                if (!count(Board::all())) {
                    $entry = 1;
                }
            }
            $board = Board::createBoard($request->get('title'), $request->get('drag_to'), $entry);
            $board_id = str_replace(' ', '-', strtolower($board->title)) . "-" . $board->id;
            $board->updateBoard(['board_id' => $board_id]);
            return Redirect::route('tasks.index');
        }
    }

    public function edit(Request $request)
    {
        $boards = Board::all();
        $board = Board::getBoardById($request->get('id'));
        $selectedDrags = $board->selectedBoardsDecode();
        $modalHtml = (string)View::make('layouts/editBoardInnerHtml',compact('boards', 'board', 'selectedDrags'));
        echo json_encode($modalHtml);
    }

    public function update(Request $request)
    {
        $validator = $this->validator($request->all());
        if ($validator->fails()) {
            return Redirect::route('tasks.index')->withErrors($validator);
        } else {
            $entry = $request->get('entry') == "on" ? 1 : null;
            if ($entry) {
                Board::resetBoardEntries();
            } else {
                if (!count(Board::all())) {
                    $entry = 1;
                }
            }
            $board_id = str_replace(' ', '-', strtolower($request->get('title'))) . "-" . $request->get('id');
            $board = Board::getBoardById($request->get('id'));
            $board->updateBoard([
                'title' => $request->get('title'),
                'drag_to' => json_encode($request->get('drag_to')),
                'entry' => $entry,
                'board_id' => $board_id,
                'updated_at' => Carbon::now()
            ]);
            return Redirect::route('tasks.index');
        }
    }

    public function delete(Request $request)
    {
        return Board::deleteBoard($request->get('id'));
    }

    public function restore(Request $request)
    {
        return Board::restoreBoard($request->get('id'));
    }

    public function order(Request $request)
    {
        Board::orderBoards($request->get('data'));
    }

    private function validator(array $data)
    {
        return Validator::make($data, [
            'title' => ['required', 'string', 'max:255'],
            'drag_to' => ['']
        ]);
    }
}
