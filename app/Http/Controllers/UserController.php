<?php

namespace App\Http\Controllers;

use App\Models\Role;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public function index()
    {
        $users = User::all();
        return view('users/index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $roles = Role::all();
        $user = User::find($id);
        return view('users/edit', compact('user', 'roles'));
    }


    public function update(Request $request)
    {
        $validator = $this->validator($request->all());
        if ($validator->fails()) {
            return Redirect::route('users.edit', [$request->get('id')])->withErrors($validator);
        } else {
            $user = User::getUserById($request->get('id'));
            if (!Hash::check($request->get('password'), $user->password)) {
                session()->flash('password_error', 'Old password does not match!');
                return Redirect::route('users.edit', [$request->get('id')]);
            } else {
                $update_data = [
                    'name' => $request->get('name'),
                    'email' => $request->get('email'),
                    'role_id' => $request->get('role_id'),
                    'updated_at' => Carbon::now()
                ];
                if ($request->get('new_password')) {
                    $new_password = Hash::make($request->get('new_password'));
                    $update_data['password'] = $new_password;
                }
                $user->fill($update_data);
                $user->save();
                session()->flash('success', 'User updated successfully!');
                return Redirect::route('users.edit', [$request->get('id')]);
            }
        }
    }

    public function delete(Request $request)
    {
        if (User::updateUserById($request->get('id'), ['deleted' => 1]))
            return json_encode(['success' => true]);
        return json_encode(['success' => false]);
    }

    public function restore(Request $request)
    {
        if (User::updateUser($request->input('id'), ['deleted' => 2]))
            return json_encode(['success' => true]);
        return json_encode(['success' => false]);
    }

    private function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255'],
            'password' => ['required', 'string', 'min:8'],
            'new_password' => [''],
            'role_id' => ['numeric', 'required']
        ]);
    }
}
