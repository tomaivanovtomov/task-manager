<?php

namespace App\Http\Controllers;

use App\Models\Board;
use App\Models\Task;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;

class TaskController extends Controller
{
    public function index()
    {
        $boards = Board::getBoards();
        $users = User::all();
        $kanbanBoards = [];
        foreach ($boards as $board) {
            if (!auth()->user()->hasRole([1]) && $board->deleted) {
                continue;
            }
            $tasks = [];
            if ($board->tasks) {
                foreach ($board->tasks as $task) {
                    $tasks[] = $task->renderTask();
                }
            }
            $kanbanBoards[] = $board->renderBoard($tasks);
        }
        $kanbanBoards = json_encode($kanbanBoards);

        return view('tasks/index', compact('boards', 'kanbanBoards', 'users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $validator = $this->validator($request->all());
        if ($validator->fails()) {
            return Redirect::route('tasks.index')->withErrors($validator);
        } else {
            if (!Board::getEntryBoard()) {
                session()->flash('no_entry', 'No entry board set!');
                return Redirect::route('tasks.index');
            }
            Task::createTask($request->get('title'), $request->get('description'), $request->get('doer_id'));
            return Redirect::route('tasks.index');
        }
    }

    public function edit(Request $request)
    {
        $users = User::all();
        $task = Task::getTaskById($request->get('id'));
        $modalHtml = (string)View::make('layouts/editTaskInnerHtml',compact('users', 'task'));
        echo json_encode($modalHtml);
    }

    public function update(Request $request)
    {
        $validator = $this->validator($request->all());
        if ($validator->fails()) {
            return Redirect::route('tasks.index')->withErrors($validator);
        } else {
            $data = [
                'id' => $request->get('id'),
                'title' => $request->get('title'),
                'description' => $request->get('description'),
                'doer_id' => $request->get('doer_id')
            ];
            Task::updateTaskById($request->get('id'), $data);
            return Redirect::route('tasks.index');
        }
    }

    public function delete(Request $request)
    {
        Task::deleteTask($request->get('id'));
    }

    public function restore(Request $request)
    {
        Task::restoreTask($request->get('id'));
    }

    public function order(Request $request)
    {
        foreach ($request->get('data') as $key => $item) {
            $id = explode("-", $item[0]);
            $id = end($id);
            $data = [
                'id' => $id,
                'board_id' => Board::getBoardById($item[1])->id,
                'order' => $key + 1
            ];
            Task::updateTaskById($id, $data);
        }
    }

    private function validator(array $data)
    {
        return Validator::make($data, [
            'title' => ['required', 'string', 'max:255'],
            'description' => ['string']
        ]);
    }
}
