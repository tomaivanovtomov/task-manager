<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Board extends Model
{
    use HasFactory;

    protected $table = 'boards';

    protected $fillable = [
        'drag_to',
        'title',
        'board_id',
        'entry',
        'updated_at'
    ];

    public function tasks()
    {
        return $this->hasMany(Task::class)->orderBy('order');
    }

    public static function getBoards()
    {
        return self::orderBy('order')->get();
    }

    public function renderBoard($tasks)
    {
        $board_classes[] = 'kanban-board-class';
        $action_icon = "<i data-id='{$this->id}' data-type='delete' data-url='" . route('boards.delete') . "' class='delete-board ti-trash'></i>";
        if ($this->deleted == 1) {
            $action_icon = "<i data-id='{$this->id}' data-type='restore' data-url='" . route('boards.restore') . "' class='delete-board ti-trash'></i>";
            $board_classes[] = 'deleted-board';
        }
        $kanbanBoard = [
            'id' => $this->board_id,
            'title' => $this->title . (auth()->user()->hasRole([1, 2]) ? "<span class='board-action'>" . "<i data-id='{$this->id}' data-url='" . route('boards.edit') . "' class='ti-pencil edit-board'></i>" . (auth()->user()->hasRole([1]) ? "&nbsp;&nbsp; $action_icon" : "") . "</span>" : ""),
            'class' => implode(",", $board_classes),
            'dragTo' => json_decode($this->drag_to),
            'item' => $tasks
        ];
        $board_classes = [];
        return $kanbanBoard;
    }

    public static function getEntryBoard()
    {
        return self::where('entry', 1)->first();
    }

    public static function getBoardById($id)
    {
        return Board::where('id', $id)->first();
    }

    public static function resetBoardEntries()
    {
        return Board::where('entry', 1)->update(['entry' => null]);
    }

    public static function deleteBoard($id)
    {
        return Board::where('id', $id)->update(['deleted' => 1]);
    }

    public static function restoreBoard($id)
    {
        return Board::where('id', $id)->update(['deleted' => null]);
    }

    public static function orderBoards($boards)
    {
        foreach ($boards as $item) {
            Board::where('board_id', $item[0])->update(['order' => $item[1]]);
        }
    }

    public static function createBoard($title, $drag_to, $entry)
    {
        return Board::create([
            'title' => $title,
            'drag_to' => json_encode($drag_to),
            'entry' => $entry
        ]);
    }

    public function updateBoard($data)
    {
        $this->fill($data);
        return $this->save();
    }

    public function selectedBoardsDecode()
    {
        return $this->drag_to ? (gettype($this->drag_to) == 'string' ? (array)json_decode($this->drag_to) : json_decode($this->drag_to)) : [];
    }
}
