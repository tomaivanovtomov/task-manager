<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    use HasFactory;

    protected $table = 'tasks';

    protected $fillable = [
        'title',
        'description',
        'doer_id',
        'creator_id',
        'board_id',
        'done',
        'deleted'
    ];

    public function board()
    {
        return $this->belongsTo(Board::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'doer_id');
    }

    public function renderTask()
    {
        if ((!auth()->user()->hasRole([1, 2]) && $this->deleted) || (auth()->user()->hasRole([3]) && $this->doer_id != auth()->user()->id && $this->doer_id)) {
            return [];
        }
        $task_classes[] = 'kanban-task-class';
        $task_action_icon = "<i data-id='{$this->id}' data-type='delete' data-url='" . route('tasks.delete') . "' class='delete-task ti-trash'></i>";
        if ($this->deleted == 1) {
            $task_action_icon = "<i data-id='{$this->id}' data-type='restore' data-url='" . route('tasks.restore') . "' class='delete-task ti-trash'></i>";
            $task_classes[] = 'deleted-task';
        }
        $doer = $this->user ? $this->user->email : 'Not assigned';
        $task_record = [
            'id' => str_replace(' ', '-', strtolower($this->title)) . "-" . $this->id,
            'title' => $this->title . (auth()->user()->hasRole([1, 2]) ? "<span class='task-action'>" . "<i data-id='{$this->id}' data-url='" . route('tasks.edit') . "' class='ti-pencil edit-task'></i>&nbsp;&nbsp; $task_action_icon</span>" : "") . "<div class='task-doer' data-toggle='tooltip'  title='{$doer}'>{$doer}</div>",
            'class' => $task_classes
        ];
        $task_classes = [];
        return $task_record;
    }

    public static function createTask($title, $description, $doer_id, $creator_id = null)
    {
        return Task::create([
            'title' => $title,
            'description' => $description,
            'creator_id' => is_null($creator_id) ? auth()->user()->id : $creator_id,
            'doer_id' => $doer_id ?? null,
            'board_id' => Board::where('entry', 1)->first()->id
        ]);
    }

    public static function getTaskById($id)
    {
        return Task::where('id', $id)->first();
    }

    public static function updateTaskById($id, $data)
    {
        return Task::where('id', $id)->update($data);
    }

    public static function deleteTask($id)
    {
        return Task::where('id', $id)->update(['deleted' => 1]);
    }

    public static function restoreTask($id)
    {
        return Task::where('id', $id)->update(['deleted' => null]);
    }
}
