@extends('layouts.app')

@section('content')
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">Users</h4></div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <ol class="breadcrumb">
                            <li><a href="/">Dashboard</a></li>
                            <li><a href="#">Users</a></li>
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <div class="col-sm-12">
                    <div class="white-box">
                        <div class="table-responsive">
                            <table class="table color-bordered-table theme-bordered-table">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Role</th>
                                    @if(auth()->user()->hasRole([1]))
                                        <th></th>
                                        <th></th>
                                    @endif
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($users as $k => $user)
                                    <tr class="row{{ $user->id }} @if($user->deleted == 1) deleted-record @endif">
                                        <td>{{ $k+1 }}</td>
                                        <td>{{ $user->name }}</td>
                                        <td>{{ $user->email }}</td>
                                        <td>{{ $user->role->name }}</td>
                                        @if(auth()->user()->hasRole([1]))
                                            <td><a href="{{ route('users.edit', ['id' => $user->id]) }}"><i data-toggle="tooltip" title="Edit" class="action-theme-button ti-pencil"></i></a></td>
                                            @if($user->deleted == 2)
                                                <td class="delete-cell-wrapper{{ $user->id }}"><i data-toggle="tooltip" title="Delete" data-id="{{$user->id}}" data-route-delete="{{ route('users.delete') }}" data-route-restore="{{ route('users.restore') }}" class="action-theme-button ti-trash delete-record"></i></td>
                                            @else
                                                <td class="restore-cell-wrapper{{ $user->id }}"><i data-toggle="tooltip" title="Restore" data-id="{{$user->id}}" data-route-restore="{{ route('users.restore') }}" data-route-delete="{{ route('users.delete') }}" class="restore-theme-button ti-check restore-record"></i></td>
                                            @endif
                                        @endif
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
