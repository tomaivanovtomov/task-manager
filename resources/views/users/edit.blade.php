@extends('layouts.app')

@section('content')
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">{{ $user->name }} (ID: {{ $user->id }})</h4></div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <ol class="breadcrumb">
                            <li><a href="/">Dashboard</a></li>
                            <li><a href="{{ route('users.index') }}">Users</a></li>
                            <li><a href="#">User: {{ $user->id }}</a></li>
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <div class="col-sm-12">
                    @if(session('success'))
                        <div class="success-update">{{ session('success') }}</div>
                    @endif
                    <form action="{{ route('users.update') }}" method="post" id="user-update-form">
                        @csrf
                        <input type="hidden" name="id" value="{{ $user->id }}">
                        <div class="white-box">
                            <div class="row">
                                <div class="row">
                                    <div class="form-group col-sm-4">
                                        <label class="control-label">Name</label>
                                        <input required class="form-control" type="text" name="name"
                                               value="{{ $user->name }}">
                                        @error('name')
                                        <div class="error-form">{{$message}}</div>
                                        @enderror
                                    </div>
                                    <div class="form-group col-sm-4">
                                        <label class="control-label">Email</label>
                                        <input required class="form-control" type="text" name="email"
                                               value="{{ $user->email }}">
                                        @error('email')
                                        <div class="error-form">{{$message}}</div>
                                        @enderror
                                    </div>
                                    <div class="form-group col-sm-4">
                                        <label class="control-label">Role</label>
                                        <select required name="role_id" id="role" class="form-control">
                                            @foreach($roles as $role)
                                                <option
                                                    {{ $role->id == $user->id ? 'selected' : '' }} value="{{ $role->id }}">{{ $role->name }}</option>
                                            @endforeach
                                        </select>
                                        @error('role')
                                        <div class="error-form">{{$message}}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-sm-6">
                                        <label class="control-label">Password</label>
                                        <input class="form-control" type="password" required placeholder="Password"
                                               name="password">
                                        @error('password')
                                        <div class="error-form">{{$message}}</div>
                                        @enderror
                                        @if(session('password_error'))
                                            <div class="error-form">{{session('password_error')}}</div>
                                        @endif
                                    </div>
                                    <div class="form-group col-sm-6">
                                        <label class="control-label">New Password</label>
                                        <input class="form-control" type="password" placeholder="New Password"
                                               name="new_password">
                                        @error('new_password')
                                        <div class="error-form">{{$message}}</div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="row">
                                    <div class="col-sm-2">
                                        <button class="btn theme-btn">Save</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
