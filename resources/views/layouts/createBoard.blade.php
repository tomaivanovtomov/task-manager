<div id="createBoard" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Create Board</h4>
            </div>
            <div class="modal-body">
                <form action="{{ route('boards.create') }}" method="post">
                    @csrf
                    <div class="form-group">
                        <label for="" class="control-label">Title</label>
                        <input type="text" name="title" required class="form-control" placeholder="Title">
                    </div>
                    <div class="form-group">
                        <label for="" class="control-label">Drag To</label>
                        <select class="multiselect" name="drag_to[]" id="dragTo" multiple>
                            @foreach($boards as $board)
                                <option value="{{ $board->board_id }}">{{ $board->title }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="entry_check" class="control-label cpointer">Entry Board</label>
                        <input type="checkbox" id="entry_check" name="entry" class="form-control checkbox-custom cpointer"/>
                        <div>
                            <small>(Tasks will be automatically append here)</small>
                        </div>
                    </div>
                    <div class="form-group">
                        <button class="btn theme-btn">Create</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
