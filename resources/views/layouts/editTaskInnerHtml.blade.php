<div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Edit Task</h4>
        </div>
        <div class="modal-body">
            <form action="{{ route('tasks.update') }}" method="post">
                @csrf
                <input type="hidden" value="{{ $task->id }}" name="id" />
                <div class="form-group">
                    <label for="" class="control-label">Title</label>
                    <input type="text" name="title" value="{{ $task->title }}" required class="form-control" placeholder="Title">
                </div>
                <div class="form-group">
                    <label for="" class="control-label">Description</label>
                    <textarea name="description" class="form-control" placeholder="Description">{{ $task->description }}</textarea>
                </div>
                <div class="form-group">
                    <label for="" class="control-label">Assign To</label>
                    <select name="doer_id" class="multiselect-edit" id="assignTo">
                        <option value=""></option>
                        @foreach($users as $user)
                            <option {{ $user->id == $task->doer_id ? "selected" : "" }} value="{{ $user->id }}">{{ $user->email }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <button class="btn theme-btn">Update</button>
                </div>
            </form>
        </div>
    </div>
</div>
