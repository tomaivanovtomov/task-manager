<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse slimscrollsidebar">
        <ul class="nav" id="side-menu">
            <li class="user-pro">
                <a href="#" class="waves-effect"><img src="{{ asset('admin/plugins/images/users/1.jpg') }}"
                                                      alt="user-img" class="img-circle"> <span class="hide-menu">{{ auth()->user()->name }}<span
                            class="fa arrow"></span></span>
                </a>
                <ul class="nav nav-second-level">
                    <li><a href="{{ route('logout') }}"><i class="fa fa-power-off"></i> Logout</a></li>
                </ul>
            </li>

            <li>
                <a href="/" class="waves-effect">
                    <i class="ti-dashboard p-r-10"></i> <span class="hide-menu">Dashboard</span>
                </a>
            </li>
            @if(auth()->user()->hasRole([1]))
                <li>
                    <a href="{{ route('roles.index') }}" class="waves-effect">
                        <i class="ti-power-off p-r-10"></i> <span class="hide-menu">Roles</span>
                    </a>
                </li>
            @endif
            @if(auth()->user()->hasRole([1, 2]))
                <li>
                    <a href="{{ route('users.index') }}" class="waves-effect">
                        <i class="ti-user p-r-10"></i> <span class="hide-menu">Users</span>
                    </a>
                </li>
            @endif
            <li>
                <a href="{{ route('tasks.index') }}" class="waves-effect">
                    <i class="ti-list p-r-10"></i> <span class="hide-menu">Tasks</span>
                </a>
            </li>
        </ul>

    </div>
</div>
