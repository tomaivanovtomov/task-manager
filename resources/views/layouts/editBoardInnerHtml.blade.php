<div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Edit Board</h4>
        </div>
        <div class="modal-body">
            <form action="{{ route('boards.update') }}" method="post">
                @csrf
                <input type="hidden" value="{{ $board->id }}" name="id" />
                <div class="form-group">
                    <label for="" class="control-label">Title</label>
                    <input type="text" name="title" value="{{ $board->title }}" required class="form-control" placeholder="Title">
                </div>
                <div class="form-group">
                    <label for="" class="control-label">Drag To</label>
                    <select class="multiselect-edit" name="drag_to[]" id="dragTo" multiple>
                        @foreach($boards as $brd)
                            <option {{ in_array($brd->board_id, $selectedDrags) ? "selected" : "" }} value="{{ $brd->board_id }}">{{ $brd->title }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="entry_check" class="control-label cpointer">Entry Board</label>
                    <input type="checkbox" id="entry_check" {{ $board->entry == 1 ? "checked" : "" }} name="entry" class="form-control checkbox-custom cpointer"/>
                    <div>
                        <small>(Tasks will be automatically append here)</small>
                    </div>
                </div>
                <div class="form-group">
                    <button class="btn theme-btn">Update</button>
                </div>
            </form>
        </div>
    </div>
</div>
