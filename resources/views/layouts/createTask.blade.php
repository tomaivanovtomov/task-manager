<div id="createTask" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Create Task</h4>
            </div>
            <div class="modal-body">
                <form action="{{ route('tasks.create') }}" method="post">
                    @csrf
                    <div class="form-group">
                        <label for="" class="control-label">Title</label>
                        <input type="text" name="title" required class="form-control" placeholder="Title">
                    </div>
                    <div class="form-group">
                        <label for="" class="control-label">Description</label>
                        <textarea name="description" class="form-control" placeholder="Description"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="" class="control-label">Assign To</label>
                        <select name="doer_id" class="multiselect" id="assignTo">
                            <option value=""></option>
                            @foreach($users as $user)
                                <option value="{{ $user->id }}">{{ $user->email }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <button class="btn theme-btn">Create</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
