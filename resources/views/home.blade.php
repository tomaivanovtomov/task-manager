@extends('layouts.app')

@section('content')
    <!-- Page Content -->
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row bg-title">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h4 class="page-title">Dashboard</h4></div>
                <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                    <ol class="breadcrumb">
                        <li><a href="/">Dashboard</a></li>
                    </ol>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- .row -->
            <div class="row">
                @if(auth()->user()->hasRole([1]))
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="white-box">
                        <h3 class="box-title">Total Users</h3>
                        <div class="text-right"><span class="text-muted"></span>
                            <h1><sup><i class="text-success"></i></sup> {{ $users }}</h1>
                        </div>
                    </div>
                </div>
                @endif
                @if(auth()->user()->hasRole([1, 2]))
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="white-box">
                        <h3 class="box-title">Total Tasks</h3>
                        <div class="text-right"><span class="text-muted"></span>
                            <h1><sup><i class="text-danger"></i></sup> {{ $tasks }}</h1>
                        </div>
                    </div>
                </div>
                @endif
                @if(auth()->user()->hasRole([3]))
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="white-box">
                        <h3 class="box-title">Assigned Tasks</h3>
                        <div class="text-right"><span class="text-muted"></span>
                            <h1><sup><i class="text-info"></i></sup> {{ $user_tasks }}</h1>
                        </div>
                    </div>
                </div>
                @endif
            </div>
        </div>
    </div>
@endsection
