@extends('layouts.app')

@section('content')
    <section id="wrapper" class="login-register">
        <div class="login-box">
            <div class="white-box">
                <form class="form-horizontal form-material" id="resetform" action="{{route('password.update')}}" method="post">
                    @csrf
                    <input type="hidden" name="token" value="{{ $token }}">
                    <h3 class="box-title m-b-20">Reset Password</h3>
                    <div class="form-group ">
                        <div class="col-xs-12">
                            <input class="form-control @error('email') is-invalid @enderror" type="text" required="" placeholder="Email" name="email">
                        </div>
                    </div>

                    <div class="form-group ">
                        <div class="col-xs-12">
                            <input class="form-control @error('password') is-invalid @enderror" type="password" required="" placeholder="Password" name="password">
                        </div>
                    </div>

                    <div class="form-group ">
                        <div class="col-xs-12">
                            <input class="form-control @error('email') is-invalid @enderror" type="password" required="" placeholder="Confirm Password" name="password_confirmation">
                        </div>
                    </div>

                    @foreach($errors->all() as $error)
                        <div class="error-form">{{$error}}</div>
                    @endforeach

                    <div class="form-group text-center m-t-20">
                        <div class="col-xs-12">
                            <button class="btn btn-info btn-lg btn-block waves-effect waves-light" type="submit">Reset Password</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection
