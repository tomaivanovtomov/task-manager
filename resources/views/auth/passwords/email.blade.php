@extends('layouts.app')

@section('content')
    <section id="wrapper" class="login-register">
        <div class="login-box">
            <div class="white-box">
                <form class="form-horizontal form-material" id="resetlinkform" action="{{route('password.email')}}" method="post">
                    @csrf
                    <h3 class="box-title m-b-20">Password Reset Form</h3>
                    <div class="form-group ">
                        <div class="col-xs-12">
                            <input class="form-control @error('email') is-invalid @enderror" type="text" required="" placeholder="Email" name="email">
                        </div>
                    </div>

                    @foreach($errors->all() as $error)
                        <div class="error-form">{{$error}}</div>
                    @endforeach

                    @if(session('status'))
                        <div class="success-link">{{session('status')}}</div>
                    @endif

                    <div class="form-group text-center m-t-20">
                        <div class="col-xs-12">
                            <button class="btn btn-info btn-lg btn-block waves-effect waves-light" type="submit">Send Reset Link</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection
