@extends('layouts.app')
<style>
    #page-wrapper {
        background: white !important;
    }
</style>
@section('content')
    <div id="page-wrapper">
        <div class="container-fluid" style="background: white">
            <div class="row bg-title">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h4 class="page-title">Tasks</h4></div>
                <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                    <ol class="breadcrumb">
                        <li><a href="/">Dashboard</a></li>
                        <li><a href="#">Tasks</a></li>
                    </ol>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            @if(auth()->user()->hasRole([1, 2]))
            <div class="form-group col-sm-3">

                @if(auth()->user()->hasRole([1]))
                <button class="btn theme-btn m-l-15" data-toggle="modal" data-target="#createBoard">Create Board</button>
                @endif

                <button class="btn theme-btn m-l-15" data-toggle="modal" data-target="#createTask">Create Task</button>

                @if(session('no_entry'))
                <div class="error-form custom-left m-l-15 m-t-10">{{ session('no_entry') }}</div>
                @endif

                @foreach($errors->all() as $error)
                    <div class="error-form">{{$error}}</div>
                @endforeach
            </div>
            @endif
            <div class="form-group col-sm-12 kanban-wrapper">
                <div class="kanban-board" style="background: white">

                </div>
            </div>
        </div>
    </div>
@endsection


@include('layouts.createBoard', ['boards' => $boards])

@include('layouts.editBoard')

@include('layouts.createTask', ['users' => $users])

@include('layouts.editTask')

<script>
    let dragBoardPermission = '<?= auth()->user()->hasRole([1, 2]) ?>'
    let boards = <?= $kanbanBoards ?>
</script>
